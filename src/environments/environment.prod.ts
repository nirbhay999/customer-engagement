export const environment = {
  production: true,
  baseUrl:"https://i67qciyx2h.execute-api.ap-south-1.amazonaws.com/Stage/",
  openWeatherCityId:{
    pune:"1259229",
    finland:"660013",
    bangalore:"1277333",
    ostrava:"3068799"
  },
  cognitoDefaultUserPassword:"Hrt@5678$lopI",
  region: 'us-east-1',

    identityPoolId: 'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
    userPoolId: 'ap-south-1_RrcUqLOYg',
    clientId: '5sq86s0oo6mr1qb3lj9iut2sth',

    rekognitionBucket: 'rekognition-pics',
    albumName: "usercontent",
    bucketRegion: 'us-east-1',

    ddbTableName: 'LoginTrail',

    cognito_idp_endpoint: '',
    cognito_identity_endpoint: '',
    sts_endpoint: '',
    dynamodb_endpoint: '',
    s3_endpoint: ''
};
