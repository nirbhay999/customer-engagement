import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresenterprofileComponent } from './presenterprofile.component';

describe('PresenterprofileComponent', () => {
  let component: PresenterprofileComponent;
  let fixture: ComponentFixture<PresenterprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresenterprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresenterprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
