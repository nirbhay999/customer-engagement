import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-presenterprofile',
  templateUrl: './presenterprofile.component.html',
  styleUrls: ['./presenterprofile.component.css']
})
export class PresenterprofileComponent implements OnInit {

  user:any={};
  constructor(private common:CommonService,private route:ActivatedRoute) {
    this.user.UserId="";
    this.common.pageTitle.next("Who am I");
   }
  ngOnInit() {
    var id = this.route.snapshot.params["id"];
    this.user=this.common.userProfiles.find(x=>x.UserId==id);
  }

}
