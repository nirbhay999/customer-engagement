import { Component, OnInit,NgZone, ChangeDetectionStrategy   } from '@angular/core';
import {EventService} from "../services/event.service"
import {ToastrService} from "../services/toastr.service";
import * as moment from 'moment';
import "moment-timezone";
import {CommonService} from "../services/common.service";
import { ActivatedRoute } from '@angular/router';
import { trigger, transition, style, animate, query, stagger, keyframes } from '@angular/animations';
declare var $:any

interface IDaySchedule{
  date?:any,
  dayNo?:number,
  day?:number,
  month?:number,
  offeringSchedule?:any[]
}
@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), {optional: true}),
        query(':enter', stagger('200ms', [
          animate('400ms ease-in', keyframes([
            style({opacity: 0, transform: 'translateX(-75%)', offset: 0}),
            style({opacity: 1, transform: 'translateX(0)',     offset: 1.0}),
          ]))]), {optional: true})
      ])
    ])
  ],
})
export class AgendaComponent implements OnInit {
  techSpecMeta = {make: null};
  dayWiseSchedule:IDaySchedule[]=[];
  selectedDay:IDaySchedule={};
  constructor(private eventService:EventService,private toastr:ToastrService,
    private common:CommonService,
    private router:ActivatedRoute) { 
      this.common.loadingText.next("Finding Agendas");
      this.common.pageTitle.next("Agenda Calendar");
      this.dayWiseSchedule=[];
    }


  ngOnInit() {
    
  this.eventService.GetEventAgenda({"EventId":this.router.snapshot.params["id"]}).subscribe(result=>{
   
    this.processData(result);
    this.selectedDay = this.dayWiseSchedule[0];
    },error=>{
      this.toastr.error(error);
    },()=>{
    });
  }
  setPresenterImage(image:string){
    this.common.presenterImage.next(image);
  }
toggle(id:string){
  let jid="#" + "agenda-" + id;
  $(jid).toggle("fast");
  $(jid).parent().parent().find(".show-content").toggle();
}
  setSelectedDay(day:number){
    this.selectedDay = this.dayWiseSchedule.filter(x=>x.dayNo===day)[0];
  }
  processData(result:any){
    let dayNo=1;
    result.forEach(item=>{
      this.common.cacheUserProfile(item.Offering.User);
      let utcFromDateTime = item.FromDateTime + "";
      let utcToDateTime = item.ToDateTime + "";
      let dayOfAgenda = moment(utcFromDateTime).local().date();
      let monthOfAgenda =  moment(utcFromDateTime).local().month();
        var sameDayEvent = this.dayWiseSchedule.filter(x=>x.day===dayOfAgenda && x.month === monthOfAgenda);
        if(sameDayEvent.length===1){
          sameDayEvent[0].offeringSchedule.push({
            "title":item.Content,
            "shortDescription":item.Offering.ShortDescription,
            "startTime":moment(utcFromDateTime).add(5.5,"hours").format('LT'),
            "endTime":moment(utcToDateTime).add(5.5,"hours").format('LT'),
            "defaultOfferingDuration":item.Offering.Duration,
            "linkToPPT":item.Offering.Ppt,
            "linkToVideo":item.Offering.Video,
            "longDescription":item.Offering.LongDescription,
            "highlights":item.Offering.KeyHighlights,
            "spec":item.Offering.PresentorSpecialization,
            "presentorName":item.Offering.User.FirstName + " " + item.Offering.User.LastName,
            "designation":item.Offering.User.Designation,
            "photo":item.Offering.User.Photo,
            "userId":item.Offering.User.UserId,
            "id":item.AgendaId,
            "meetingRoom":item.MeetingRoomName||"Location not yet final"
          })
        }else{
          this.dayWiseSchedule.push({
            date:moment(utcFromDateTime).local().format('ll'),
            dayNo:dayNo,
            day:dayOfAgenda,
            month:monthOfAgenda,
            offeringSchedule:[{
            "title":item.Content,
            "shortDescription":item.Offering.ShortDescription,
            "startTime":moment(utcFromDateTime).add(5.5,"hours").format('LT'),
            "endTime":moment(utcToDateTime).add(5.5,"hours").format('LT'),
            "defaultOfferingDuration":item.Offering.Duration,
            "linkToPPT":item.Offering.Ppt,
            "linkToVideo":item.Offering.Video,
            "longDescription":item.Offering.LongDescription,
            "highlights":item.Offering.KeyHighlights,
            "spec":item.Offering.PresentorSpecialization,
            "presentorName":item.Offering.User.FirstName + " " + item.Offering.User.LastName,
            "designation":item.Offering.User.Designation,
            "photo":item.Offering.User.Photo,
            "userId":item.Offering.User.UserId,
            "id":item.AgendaId,
            "meetingRoom":item.MeetingRoomName||"Location not yet final"
            }]
          });
          dayNo = dayNo + 1;
        }
    });
  }
}
