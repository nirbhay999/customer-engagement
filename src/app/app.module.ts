import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './/app-routing.module';
import { AppComponent } from './app.component';
import { SideMenuComponent } from './shared/components/sidemenu/sidemenu.component'
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Tieto50yearsComponent } from './tieto50years/tieto50years.component';
import { EventsComponent } from './events/events.component';
import { AbouttietoComponent } from './abouttieto/abouttieto.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgendaComponent } from './agenda/agenda.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { FeedbackComponent } from './feedback/feedback.component';
import { DataService } from './services/dataService';
import { Configuration } from './app.constants';
import { MatSliderModule } from '@angular/material/slider';
import { MatInputModule } from '@angular/material/input';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AboutpresenterComponent } from './aboutpresenter/aboutpresenter.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { WeatherComponent } from './weather/weather.component';
import { WhoswhoComponent } from './whoswho/whoswho.component';
import { DosdontComponent } from './dosdont/dosdont.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PresenterprofileComponent } from './presenterprofile/presenterprofile.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import {CognitoUtil} from "./services/cognito.service";
import {AwsUtil} from "./services/aws.service";
import {UserLoginService} from "./services/user-login.service";
import {UserParametersService} from "./services/user-parameter.service";
import {UserRegistrationService} from "./services/user-registration.service";
import { Forgotpasswordstep1Component } from './auth/forgot/forgotpasswordstep1/forgotpasswordstep1.component';
import { FormsModule } from '@angular/forms';
import { LogoutComponent } from './auth/logout/logout.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import {LoaderInterceptorService } from "./services/http-interceptor.service";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { DemoDay1Component } from './demo-day1/demo-day1.component';

@NgModule({
  declarations: [
    AppComponent,
    SideMenuComponent,
    LoginComponent,
    DashboardComponent,
    Tieto50yearsComponent,
    EventsComponent,
    AbouttietoComponent,
    ContactusComponent,
    AgendaComponent,
    FeedbackComponent,
    WelcomeComponent,
    AboutpresenterComponent,
    WeatherComponent,
    WhoswhoComponent,
    DosdontComponent,
    PreferencesComponent,
    PresenterprofileComponent,
    Forgotpasswordstep1Component,
    LogoutComponent,
    DemoDay1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatGridListModule,
    MatSliderModule,
    MatInputModule,
    MatCheckboxModule,
    MatSnackBarModule,
    HttpModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatRippleModule,
    MatTooltipModule,
    FormsModule,
    NgxSpinnerModule
  ],
  entryComponents: [
    AgendaComponent,
    FeedbackComponent

  ],
  providers: [
    Configuration,
    DataService,
    CognitoUtil,
    AwsUtil,
    UserRegistrationService,
    UserLoginService,
    UserParametersService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass:LoaderInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }
