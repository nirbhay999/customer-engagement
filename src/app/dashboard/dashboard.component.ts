import { Component, OnInit } from '@angular/core';
import {environment} from "../../environments/environment";
import {EventService} from "../services/event.service";
import {ToastrService} from "../services/toastr.service";
import { trigger, transition, style, animate, query, stagger, keyframes } from '@angular/animations';
import * as moment from 'moment';
import "moment-timezone";
import {CommonService} from "../services/common.service";
import { fadeIn } from '../animations';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), {optional: true}),
        query(':enter', stagger('100ms', [
          animate('300ms ease-in', keyframes([
            style({opacity: 0, transform: 'translateX(-75%)', offset: 0}),
            style({opacity: 1, transform: 'translateX(0)',     offset: 1.0}),
          ]))]), {optional: true})
      ])
    ])
  ],
})
export class DashboardComponent implements OnInit {
  public events:any[]=[];
  public nearestEvent:any={};
  public diff:number;
  public dayOfweek:string;
  public rand = Math.floor((Math.random() + 0.2) * 12);
  constructor(private eventService:EventService,private toastr:ToastrService,private common:CommonService ) {
    this.common.loadingText.next("Looking for your events");
      this.common.pageTitle.next("Home");
      this.dayOfweek="";
      this.nearestEvent.Location={};
      this.nearestEvent.Location.City="";
      this.nearestEvent.Location.LocationName="";
      this.nearestEvent.EventName="No Event scheduled for you";
  }

  ngOnInit() {
    this.eventService.getHomePageData().subscribe(result=>{
      if(result && result.length===0){
        this.events=result;
      }
      if(result.length>0)
      {
      result.forEach(element => {
        this.events.push(element)
      });
      this.common.eventList=result;
      this.events.forEach((item)=>{
        item.StartDate = item.StartDate + "";
        item.EndDate = item.EndDate + "";
        item.formattedSdate = moment(item.StartDate).format("MMM Do"),
        item.formattedEdate=moment(item.EndDate).format("MMM Do")
      })
      this.nearestEvent=this.events[0];
      this.diff = moment(this.nearestEvent.StartDate).diff(moment().local(),"days");
      this.dayOfweek =  moment(this.nearestEvent.StartDate).local().format('dddd');
    }
    },error=>{
      this.toastr.error(error);
    });
   
  }
}
