import { Component, OnInit } from '@angular/core';
import {environment} from "../../environments/environment";
import {WeatherService} from "../services/weather.service";
import { CommonService } from '../services/common.service';
declare var Skycons:any;
declare var $:any;
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  temperature:number;
  maxTemp:number;
  minTemp:number;
  weatherIcon:string="";
  weatherIconPath:string="";
  description:string;
  city:string="";
  country:string="";
  humidity:string;
  pressure:string;
  windSpeed:string;


  constructor(private service:WeatherService,private common:CommonService) {
    this.common.pageTitle.next("Weather");
    this.common.loadingText.next("Getting weather report");
   }

  ngOnInit() {
  
    this.service.getWeatherData(environment.openWeatherCityId.pune).subscribe(result=>{
      this.temperature = Math.round(result.main.temp-273.15);
      this.weatherIcon=this.mapOpenWeatherToSkycons(result.weather[0].icon);
      this.description=result.weather[0].description;
      this.minTemp=Math.round(result.main.temp_min-273.15);
      this.maxTemp=Math.round(result.main.temp_max-273.15);
      this.city=result.name;
      this.country=result.sys.country;
      this.humidity=result.main.humidity + " %";
      this.pressure=result.main.pressure + " atm";
      this.windSpeed=result.wind.speed + " Km/hr";
      console.log(result);
      setTimeout(() => {
        this.setAnimatedWeatherIcons();
      }, 100); 
    })
   // $(".view-container").scrollTop(0);
    
  }

  setAnimatedWeatherIcons(){
    var icons = new Skycons({"color": "#f5f5f5"}),
    list  = [
    "clear-day","clear-night", "partly-cloudy-day",
    "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
    "fog"
    ],
    i;

  for(i = list.length; i--; )
  icons.set(list[i], list[i]);

  icons.play();
  }
mapOpenWeatherToSkycons(weatherId:string){
switch (weatherId){
  case "01d":return "clear-day";
  case "01n": return "clear-night";
  case "02d":
  case "03d":
  return "partly-cloudy-day";
  case "02n":
  case "03n":
  return "partly-cloudy-night";
  case "04d":
  case "04n":
  return "cloudy"
  case "9d":
  case "9n":
  case "10d":
  case "10n":
  return "rain";
  case "11d":
  case "11n":
  return "sleet"
  case "13d":
  case "13n":
  return "snow";
  case "50d":
  case "50n":
  return "fog";

  
  
 
}
}
}
