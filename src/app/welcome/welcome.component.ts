import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {environment} from "../../environments/environment"

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    //environment.pageTitle="Customer Engagement";
  }

  nextClicked(){
    this.router.navigateByUrl('/home');
  }

}
