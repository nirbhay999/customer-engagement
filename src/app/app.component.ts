import { Component, OnInit, ChangeDetectorRef, AfterViewChecked, OnDestroy  } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from "./services/common.service";
import {environment} from "../environments/environment";
import {MediaMatcher} from '@angular/cdk/layout';
import {style, state, animate, transition, trigger} from '@angular/animations';
import {UserLoginService} from "./services/user-login.service";
import {LoggedInCallback} from "./services/cognito.service";
import {Router} from "@angular/router";
import {UserProfileService} from "./services/user-profile.service";
import {UserParametersService} from "./services/user-parameter.service";
import {Callback} from "./services/cognito.service"
import { CognitoUserAttribute } from 'amazon-cognito-identity-js';
import {ToastrService} from "./services/toastr.service";
import {Location} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0}),
        animate('500ms', style({opacity:1})) 
      ])
    ]),
    trigger('flyInOut', [
      state('in', style({transform: 'translateY(0)'})),
      transition('void => *', [
        style({transform: 'translateY(-100%)'}),//
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({transform: 'translateY(100%)'}))
      ])
    ])
  ]
})
export class AppComponent implements OnInit, OnDestroy,LoggedInCallback,Callback {
  callback(): void {
    throw new Error("Method not implemented.");
  }
  
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  title = 'customer-engagement';
  showLoader: boolean;
  environment=environment;
  
  constructor(public commonService: CommonService, 
    public cdRef:ChangeDetectorRef,
    media: MediaMatcher,
    private userLoginService:UserLoginService,
    private router:Router,
    private userProfileService:UserProfileService,
    private userParameterService:UserParametersService,
    private toastr:ToastrService,
    private location:Location) {
      this.mobileQuery = media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => cdRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
      this.commonService.loadingText.next("Logging you in");
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  goBack(){
    this.location.back();
  }

  ngOnInit() {
    this.userLoginService.isAuthenticated(this);
  }
  isLoggedIn(message: string, loggedIn: boolean): void {
    // If user is already logged in , get logged in user parameters.
    // After the parameters are successfully fetched "callbackWithPrama" callback function
    // will be called with the result.
    if(loggedIn){
      this.userParameterService.getParameters(this);
    }else{
      // do nothing, let the application undertake a normal flow
    }
  }
  callbackWithParam(result: CognitoUserAttribute[]): void {
    let loggedInUserEmail="";
    if(result){
      console.log(result);
      result.forEach(element => {
        if(element.getName()=="email"){
          loggedInUserEmail=element.getValue();
        }
      });
      this.userProfileService.GetUserProfileData(loggedInUserEmail).subscribe((result)=>{
        this.commonService.userEmail=loggedInUserEmail;
        this.commonService.nickname.next(result.FirstName + " " + result.LastName);
        this.commonService.profileImage.next(result.Photo);
        this.commonService.setLoggedinTrue();
      this.commonService.shouldNavBarBeVisible.next(true);      
      this.router.navigate(['/home']);
      },error=>{
        this.toastr.error("Error occured while reading profile data");
      },
      ()=>{

      });
      
    }
  }
  
}
