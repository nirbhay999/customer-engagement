import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor(private httpClient:HttpClient) { }
  public GetUserProfileData(email:string):Observable<any>{ 
    return this.httpClient.post(environment.baseUrl + "GetUserByUserName",'"' + email + '"');
  }
}
