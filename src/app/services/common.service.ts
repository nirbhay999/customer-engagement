import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router, CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService implements CanActivate {
  
  constructor(private router:Router) { }
  public shouldNavBarBeVisible = new BehaviorSubject<boolean>(false);
  public profileImage=new BehaviorSubject<string>("");
  private _isLoggedIn:boolean = false;
  public userEmail:string;
  public  pageTitle = new BehaviorSubject<string>("");
  public nickname = new BehaviorSubject<string>("");
  public loadingText= new BehaviorSubject<string>("");
  public eventList:any;
  public presenterImage=new BehaviorSubject<string>("");
  public userProfiles:{UserId?:number}[]=[];
  public setLoggedinTrue(){
    this._isLoggedIn=true;
  }
  public setLoggedInFalse(){
    this._isLoggedIn=false;
  }
  public isLoggedIn(){
    return this._isLoggedIn
  }
  public cacheUserProfile(obj){
    if(!this.userProfiles.find(x=>x.UserId==obj.UserId)){
      this.userProfiles.push(obj);
    }
  }
  canActivate(){
    if(!this._isLoggedIn)
    {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

}
