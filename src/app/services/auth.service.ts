import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(true);
  constructor(private router:Router) { }
  login():void
  {
    this.loggedIn.next(true);
    this.router.navigateByUrl('/welcome');
  }
  get isLoggedIn() {
    return this.loggedIn.asObservable();

  }
}
