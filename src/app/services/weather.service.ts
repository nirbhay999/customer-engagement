import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {WeatherObject, Weather} from "../Models/weather";

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http:HttpClient) { }
  public getWeatherData(cityId:string):Observable<WeatherObject>{
    return this.http.get<WeatherObject>(`https://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=5186b370ae007b0b0adb9ac8c4c2b203`);
  }
}
