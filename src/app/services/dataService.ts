import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { catchError, tap, map } from 'rxjs/operators';
import { Configuration } from '../app.constants';

@Injectable()
export class DataService {

    private actionUrl: string;

    constructor(private http: HttpClient, private _configuration: Configuration) {
        this.actionUrl = _configuration.ServerWithApiUrl;
    }

    public getAll<T>(): Observable<T> {
        return this.http.get<T>(this.actionUrl);
    }

    public getSingle<T>(id: number): Observable<T> {
        return this.http.get<T>(this.actionUrl + id);
    }

    public add<T>(itemName: string): Observable<T> {
        const toAdd = JSON.stringify({ ItemName: itemName });

        return this.http.post<T>(this.actionUrl, toAdd);
    }

    // public addPromise<T>(itemName: string): Observable<T> {
    //     const toAdd = JSON.stringify({ ItemName: itemName });

    //     var response = this.http.post<T>(this.actionUrl, toAdd)
    //         .toPromise().then(result => {
    //             console.log('hide spinner here')
    //             let output = result;
    //             return output;
    //         })
    //         .catch((error: any) => this.onErrorHandler(error));
    //     return response;
    // }

    // public addPipe<T>(itemName: string): Observable<T> {
    //     const toAdd = JSON.stringify({ ItemName: itemName });

    //     var response = this.http.post<T>(this.actionUrl, toAdd)
    //         .pipe(tap(              
    //             data =>
    //                 console.log(JSON.stringify(data))
    //             //call spinner 
    //         ),
    //             catchError(this.handleError));
    //     return response;
    // }

    // //     return this.http
    // //   .get(API_URL + 'chamados/busca/?' + textoBusca + '?' + projeto).pipe(
    // //     map(response => {
    // //       const chamados = response.json();
    // //       return chamados.map((chamado) => new Chamado(chamado));
    // //     })
    // //     catchError(this.handleError);

    public update<T>(id: number, itemToUpdate: any): Observable<T> {
        return this.http
            .put<T>(this.actionUrl + id, JSON.stringify(itemToUpdate));
    }

    public delete<T>(id: number): Observable<T> {
        return this.http.delete<T>(this.actionUrl + id);
    }

    private onErrorHandler(error: any) {
        const errors = error.json();
        if (error.status == 406 && Array.isArray(errors) && errors.indexOf("User is not logged in.") !== -1) {
        } else if (error.status == 401 && Array.isArray(errors) && errors.indexOf("CSRF validation failed") !== -1) {
            // TODO: should logout after getting token
        }
        return Observable.throw(errors);
    }
    handleError(error: Response) {
        console.log(error);
        return Observable.throw(error);
    }
}


@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }

        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        console.log(JSON.stringify(req.headers));
        return next.handle(req);
    }
}