import { Injectable } from '@angular/core';
import {CommonService} from "./common.service";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private commonService:CommonService,
    private http:HttpClient) { }

  public getUserEvent():Observable<any>{
    return this.http.post(environment.baseUrl + "GetUserEvents",'"'+ this.commonService.userEmail + '"');
  }

  public getHomePageData():Observable<any>{
    return this. http.post("https://h2u7ucpsph.execute-api.ap-south-1.amazonaws.com/Stage/GetHomePageData",'"'+ this.commonService.userEmail + '"');
  }

  public GetEventAgenda(data:any):Observable<any>{
    return this.http.post(environment.baseUrl + "GetAgendaByEvent",data);
  }

  public GetEventUser(data:any):Observable<any>{
    return this.http.post(environment.baseUrl + "GetEventUser",data);
  }
}
