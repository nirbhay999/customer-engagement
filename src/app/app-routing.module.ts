import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Tieto50yearsComponent } from './tieto50years/tieto50years.component';
import { EventsComponent } from './events/events.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AbouttietoComponent } from './abouttieto/abouttieto.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { WeatherComponent } from './weather/weather.component';
import { AgendaComponent } from "./agenda/agenda.component";
import { FeedbackComponent } from "./feedback/feedback.component";
import { WhoswhoComponent } from "./whoswho/whoswho.component";
import { DosdontComponent } from "./dosdont/dosdont.component";
import { PreferencesComponent } from "./preferences/preferences.component"
import { PresenterprofileComponent } from "./presenterprofile/presenterprofile.component";
import { Forgotpasswordstep1Component } from "./auth/forgot/forgotpasswordstep1/forgotpasswordstep1.component";
import { LogoutComponent } from "./auth/logout/logout.component";
import {CommonService} from "./services/common.service"

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent },
  { path: 'home', component: DashboardComponent,canActivate:[CommonService]  },
  { path: 'abouttieto', component: AbouttietoComponent,canActivate:[CommonService]  },
  { path: 'tieto50years', component: Tieto50yearsComponent,canActivate:[CommonService]  },
  { path: 'event/:id', component: EventsComponent,canActivate:[CommonService]  },
  { path: 'contactus', component: ContactusComponent,canActivate:[CommonService]  },
  { path: 'welcome', component: WelcomeComponent,canActivate:[CommonService]  },
  { path: 'weather', component: WeatherComponent,canActivate:[CommonService]  },
  { path: 'agenda/:id', component: AgendaComponent,canActivate:[CommonService]  },
  { path: 'feedback', component: FeedbackComponent,canActivate:[CommonService]  },
  { path: 'whoswho/:id', component: WhoswhoComponent,canActivate:[CommonService]  },
  { path: 'dosdont', component: DosdontComponent,canActivate:[CommonService]  },
  { path: 'preferences', component: PreferencesComponent,canActivate:[CommonService]  },
  { path: 'presenterprofile/:id', component: PresenterprofileComponent,canActivate:[CommonService]  },
  { path: 'forgotpassword', component: Forgotpasswordstep1Component},
  { path: 'logout', component: LogoutComponent,canActivate:[CommonService]  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{scrollPositionRestoration:'enabled'})],
  exports: [RouterModule]
})

export class AppRoutingModule { }
