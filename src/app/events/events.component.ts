import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewChecked,
  OnDestroy
} from '@angular/core';
import {
  MatDialog,
  MAT_DIALOG_DATA
} from '@angular/material';
import {
  AgendaComponent
} from '../agenda/agenda.component';
import {
  MatSnackBar
} from '@angular/material';
import {
  $
} from 'protractor';
import { environment } from "../../environments/environment";
import { MediaMatcher } from '@angular/cdk/layout';
import { CommonService } from '../services/common.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit, AfterViewChecked {
  showAlert: boolean;
  mobileQuery: MediaQueryList;
  evenTitle: string;
  currentEvent: any;
  private _mobileQueryListener: () => void;
  constructor(public dialog: MatDialog,
    public cdRef: ChangeDetectorRef,
    media: MediaMatcher,
    private common: CommonService,
    private route: ActivatedRoute) {
    this.mobileQuery = media.matchMedia('(max-width: 800px)');
    this._mobileQueryListener = () => cdRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.common.pageTitle.next("Event");
    this.currentEvent = this.common.eventList.filter(x => x.EventId == this.route.snapshot.params["id"])[0];

  }

  ngOnInit() {

  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
}
