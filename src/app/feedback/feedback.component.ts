import { Component, OnInit } from '@angular/core';
import { CommonService } from '../services/common.service';
import {ToastrService} from "../services/toastr.service";
import {Location} from '@angular/common';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  constructor(public common:CommonService,private toastr:ToastrService,
    private location:Location) {
    this.common.pageTitle.next("Feedback");
   }

  ngOnInit() {
    
  }
  submitFeedback(){
    setTimeout(()=>{
      this.toastr.success("Feedback submitted successfully")
      this.location.back();
    },1000);
  }
  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }
}
