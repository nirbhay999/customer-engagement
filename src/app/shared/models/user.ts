export class User {
    id: number;
    name: string;
    profilepic: string;
    email:string;
    phone:string;
    username:string;
  }
  