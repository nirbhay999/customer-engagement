export class Event {
    eventId : number;
    locationId : number;
    eventName : string;
    startDate  : string;
    endDate : string;
    activeDate : string;
    eventvenue  : string;
    eventpic  : string;
    summary : string;
    status : number;
    description : string;
    createdOn : string;
    createdBy : number; 
    updatedOn : string;
    updatedBy : number;
  }

  export class EventDate {
    day : string; 
    date : string;
  }

  export class EventDetails {
    eventdates : EventDate[]; 
    events : Event[];
  }