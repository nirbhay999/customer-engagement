import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'side-menu',
  templateUrl: 'sidemenu.component.html',
  styleUrls: ['sidemenu.component.css']
})
export class SideMenuComponent {
 isLoggedIn$:boolean;
  showMenu(){
    debugger
    document.getElementById("sidebar-container").style.display="block";
    document.getElementById("collapse-menu").style.display="none";
  }
  hideMenu(){
    if(window.outerWidth<700){
    document.getElementById("sidebar-container").style.display="none";
    document.getElementById("collapse-menu").style.display="block";
    }
  }
}
