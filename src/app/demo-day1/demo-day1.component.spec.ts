import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoDay1Component } from './demo-day1.component';

describe('DemoDay1Component', () => {
  let component: DemoDay1Component;
  let fixture: ComponentFixture<DemoDay1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoDay1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoDay1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
