import { Injectable } from '@angular/core';
import { empty } from 'rxjs';

@Injectable()
export class Configuration {
    public Server = 'https://localhost:44358/api/values';
    public EndPoint = 'users/';
    public ServerWithApiUrl = this.Server ;//+ this.EndPoint;
}
