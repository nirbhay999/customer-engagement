import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbouttietoComponent } from './abouttieto.component';

describe('AbouttietoComponent', () => {
  let component: AbouttietoComponent;
  let fixture: ComponentFixture<AbouttietoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbouttietoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbouttietoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
