import { Component, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { User } from '../../shared/models/user';
import { DataService } from '../../services/dataService';
import { UserLoginService } from "../../services/user-login.service";
import { CognitoCallback, LoggedInCallback } from "../../services/cognito.service";
import { Router } from "@angular/router";
import { UserRegistrationService } from "../../services/user-registration.service"
import { ToastrService } from "../../services/toastr.service";
import { CommonService } from "../../services/common.service";
import { UserProfileService } from "../../services/user-profile.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements CognitoCallback {

  cognitoErrorCallback(error: Error, result: any): void {
    throw new Error("Method not implemented.");
  }
  cognitoSuccessCallback(message: string, result: any): void {
    throw new Error("Method not implemented.");
  }

  email: string;
  password: string;
  errorMessage: string;
  userConfirmed: boolean = true;
  confirmationCode: string = "";
  private user: User[];
  private callbackFor: string = "";
  constructor(private authService: AuthService,
    private dataService: DataService,
    private userLoginService: UserLoginService,
    private userRegService: UserRegistrationService,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private userProfileService: UserProfileService) {
    this.commonService.loadingText.next("Logging you in");
    // If the user is logged in send hom back to home
    if (this.commonService.isLoggedIn()) {
      this.router.navigate(['/home']);
    }
  }

  login(): void {
    this.userLoginService.authenticate(this.email, this.password, this);
  }
  confirmRegistraion(): void {
    this.callbackFor = "confirmReg";
    this.userRegService.confirmRegistration(this.email, this.confirmationCode, this);

  }
  resendConfirmationCode(): void {
    this.callbackFor = "resendCode";
    this.userRegService.resendCode(this.email, this);
  }
  cognitoCallback(message: string, result: any) {
    if (message != null) { //error
      if (this.callbackFor === "resendCode") {
        this.toastr.error(message);
        this.callbackFor = "";
        return;
      }
      if (this.callbackFor === "confirmReg") {
        this.toastr.error(message);
        this.callbackFor = "";
        return;
      }
      this.errorMessage = message;
      console.log("result: " + this.errorMessage);
      this.toastr.error(this.errorMessage);
      if (this.errorMessage === 'User is not confirmed.') {
        this.userConfirmed = false;
      } else if (this.errorMessage === 'User needs to set password.') {
        console.log("redirecting to set new password");
        this.router.navigate(['/home/newPassword']);
      }
    } else { //success
      if (this.callbackFor === "resendCode") {
        this.toastr.info("Confirmation code sent to registered email");
        this.callbackFor = "";
        return;
      }
      // successfull login
      this.userProfileService.GetUserProfileData(this.email).subscribe((result) => {
        this.commonService.userEmail = this.email;
        this.commonService.profileImage.next(result.Photo);
        this.commonService.setLoggedinTrue();
        this.commonService.shouldNavBarBeVisible.next(true);
        this.commonService.nickname.next(result.FirstName + " " + result.LastName);
        this.router.navigate(['/home']);
      }, error => {
        this.toastr.error("Error occured while reading profile data");
      },
        () => {

        });

    }
  }
}
