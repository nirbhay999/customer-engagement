import { Component, OnInit } from '@angular/core';
import {UserLoginService} from "../../services/user-login.service";
import {LoggedInCallback} from "../../services/cognito.service";
import {Router} from "@angular/router";
import {CommonService} from "../../services/common.service";

@Component({
  template:''
})
export class LogoutComponent implements LoggedInCallback {

  constructor(private router:Router,
    private userLoginService:UserLoginService,
    private commonService:CommonService) {
      this.userLoginService.isAuthenticated(this)
     }
     isLoggedIn(message: string, isLoggedIn: boolean) {
      if (isLoggedIn) {
          this.userLoginService.logout();
          this.commonService.setLoggedInFalse();
          this.commonService.shouldNavBarBeVisible.next(false);
          this.router.navigate(['/login']);
      }

      this.router.navigate(['/login']);
  }
}
