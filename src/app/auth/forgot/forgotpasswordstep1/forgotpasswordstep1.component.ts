import { Component, OnInit } from '@angular/core';
import {UserLoginService} from "../../../services/user-login.service";
import {CognitoCallback} from "../../../services/cognito.service";
import {Router} from "@angular/router";
import {ToastrService} from "../../../services/toastr.service";
@Component({
  selector: 'app-forgotpasswordstep1',
  templateUrl: './forgotpasswordstep1.component.html',
  styleUrls: ['../../login/login.component.css']
})
export class Forgotpasswordstep1Component implements OnInit,CognitoCallback {
  
 email:string;
 errorMessage:string;
 confirmationCode:string;
 newPassword:string;
 confirmNewPassword:string;
 step:number=1;
  constructor(private userLoginService:UserLoginService,
    private toastr:ToastrService,
    private route:Router) { }

  ngOnInit() {
  }
  getConfirmationCode(){
    this.userLoginService.forgotPassword(this.email,this);
  }
  setNewPassword(){
   if(this.newPassword!==this.confirmNewPassword){
     this.toastr.error("Passwords do not match")
     return;
   }
   this.userLoginService.confirmNewPassword(this.email,this.confirmationCode,this.newPassword,this);
  }
  cognitoErrorCallback(err: Error, result: any): void {
    if(this.step===1) // user is still on email entry screen be on that screen
    {
      this.toastr.error(err.message);
    }
    if(this.step===2) // user did not enter the correct confirmation code
    {
      // either the confirmation code is wrong or the password policy does not match
      this.toastr.error(err.message);
      this.confirmationCode="";
      this.newPassword="";
      this.confirmNewPassword="";
    }
  }
  cognitoSuccessCallback(message: string, result: any): void {
    if(this.step===2){
      this.toastr.success("Password changed successfully");
      this.route.navigateByUrl("/login");
    }
    if(this.step===1){
      this.toastr.info("Verification code sent to your email id");
      this.step=2;
    }
    
  }
}
