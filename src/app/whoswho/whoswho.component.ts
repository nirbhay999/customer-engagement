import { Component, OnInit } from '@angular/core';
import {UserProfile} from "../Models/UserProfile"
import { CommonService } from '../services/common.service';
import { EventService } from '../services/event.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from '../services/toastr.service';
import { trigger, transition, style, animate, query, stagger, keyframes } from '@angular/animations';

@Component({
  selector: 'app-whoswho',
  templateUrl: './whoswho.component.html',
  styleUrls: ['./whoswho.component.css'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':enter', style({ opacity: 0 }), {optional: true}),
        query(':enter', stagger('100ms', [
          animate('100ms ease-in', keyframes([
            style({opacity: 0, transform: 'translateX(-75%)', offset: 0}),
            style({opacity: 1, transform: 'translateX(0)',     offset: 1.0}),
          ]))]), {optional: true})
      ])
    ])
  ],
})
export class WhoswhoComponent implements OnInit {
  customers:UserProfile[]=[]
  presenters:UserProfile[]=[]
  constructor(private common:CommonService,
     private eventService:EventService,
     private router:ActivatedRoute,
     private toastr:ToastrService) { 
    this.common.pageTitle.next("Who's Who");
    this.common.loadingText.next("Searching persons");
  }

  ngOnInit() {
   this.eventService.GetEventUser({
    "eventId": this.router.snapshot.params["id"]
  }).subscribe(result=>{
    result.forEach(element => {
      var obj = element;
      this.common.cacheUserProfile(obj);
      if(element.UserId==165){
        return;
      }

      if(element.UserRoleCodeValue=="CUST"){
        this.customers.push({
          name:obj.FirstName + " " + obj.LastName,
          jobTitle:obj.Designation,
          profileImage:obj.Photo || "../../assets/default.png",
          UserId:obj.UserId
        });
      }else{
        this.presenters.push({
          name:obj.FirstName + " " + obj.LastName,
          jobTitle:obj.Designation,
          profileImage:obj.Photo || "../../assets/default.png",
          UserId:obj.UserId

        });
      }
    });
  },error=>{
    this.toastr.error("Error occured while fetching event participants");
  })
  }

}
