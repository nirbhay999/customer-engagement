// move this file to a diiferent animation folder
// when the app grows and we have lots of animations
import { state, style, animate, transition, query, stagger } from '@angular/animations';

export function fadeIn(selector = ':enter', duration = '1000ms ease-out') {
 return [
   transition('* => *', [      
     query(selector, [
       style({ opacity: 0, transform: 'translateY(-5px)'}), 
       stagger('5000ms', [
         animate(duration, style({
           opacity: 1,
           transform: 'translateY(0px)'
         }))
       ])
     ], {optional: true })
   ])
 ];
}