import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutpresenterComponent } from './aboutpresenter.component';

describe('AboutpresenterComponent', () => {
  let component: AboutpresenterComponent;
  let fixture: ComponentFixture<AboutpresenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutpresenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutpresenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
