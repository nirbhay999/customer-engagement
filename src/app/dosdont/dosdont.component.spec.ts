import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosdontComponent } from './dosdont.component';

describe('DosdontComponent', () => {
  let component: DosdontComponent;
  let fixture: ComponentFixture<DosdontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosdontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosdontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
