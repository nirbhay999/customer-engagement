import { Component, OnInit } from '@angular/core';
import {environment} from "../../environments/environment"
@Component({
  selector: 'app-dosdont',
  templateUrl: './dosdont.component.html',
  styleUrls: ['./dosdont.component.css']
})
export class DosdontComponent implements OnInit {

  dos:string[]=["Dress modestly.",
  "Prepare yourself at the earliest. Indian traffic is very unpredictable.",
  "Greet people with a big smile. Handshake with ladies especially in rural areas should be avoided.",
  "Cover yourself with travel insurance for thefts, loss, and medical.",
  "Exchange money only through authorized banks or money changers.",
  "Immunize yourself against various diseases like typhoid, malaria, hepatitis and tetanus.", 
  "Carry your medication in ample supply.",
  "Bargain while buying things from roadside hawkers.",
  "Remove your footware when visiting a place of worship."];

  donts:string[]=["Do not walk over books and paper or touch them with your feet as Indians symbolize books with goddess of learning and knowledge.",
  "Do not discuss religion.",
  "Do not preach in public or hand out facts as such activities are prohibited in India.",
  "Do not drive in Indian roads unless you have an Indian drivers license and have been trained on Indian roads.",
  "Do not purchase air, train or bus tickets through strangers or unauthorized travel agents or tour operators.",
  "Do not hire any type of transportation from unlicensed operators. In case of taxis and auto-rickshaws, try to hire from pre-paid booths." ]
  constructor() { }

  ngOnInit() {
    //environment.pageTitle="Do's & Dont's"
  }

}
